trigger ContactToAccountTrigger on Contact (before insert, before update) {
    for (Contact c : Trigger.new) {
        if (c.AccountId == NULL){
            Account a = new Account (
                Name = 'Account Test Trigger'
            );
            insert a;
            c.AccountId = a.Id;
        }
    }
}