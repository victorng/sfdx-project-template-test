trigger LeadTrigger on Lead (before insert, before update){
    if(Trigger.isBefore) {  
        for(Lead l : Trigger.new) {
           if(String.isNotBlank(l.Enquiry_Sources__c)){
                List<String> enquiries = l.Enquiry_Sources__c.split(';');
                l.CountEnquiries__c = enquiries.size();
           } else {
                l.CountEnquiries__c = 0;
           }
        }
    }
}