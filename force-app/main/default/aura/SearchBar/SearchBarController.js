/**
 * The function first gets an instance of the SearchKeyChange event.
 * It then sets the event’s searchKey parameter to the input field’s new value
 * Finally, it fires the event so that registered listeners can catch it
*/

({
     searchKeyChange: function(component, event, helper) {
         var myEvent = $A.get("e.c:SearchKeyChange");
         myEvent.setParams({"searchKey": event.target.value});
         myEvent.fire();
     }
 })