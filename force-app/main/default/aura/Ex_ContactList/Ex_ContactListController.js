/**
 * The controller has a single function called doInit. 
 * This is the function the component calls when it is initialized.
 * You first get a reference to the findAll() method in the component’s server-side controller (ContactListController), and store it in the action variable.
 * Since the call to the server’s findAll() method is asynchronous, you then register a 
 * callback function that is executed when the call returns. 
 * 
 * In the callback function, you assign the list of contacts to the component’s contacts
 * attribute.
 * $A.enqueueAction(action) sends the request the server. 
 * More precisely, it adds the call to the queue of asynchronous server calls. 
 * That queue is an optimization feature of Lightning.
*/

({
     doInit : function(component, event) {
         var action = component.get("c.findAll");
         action.setCallback(this, function(a) {
             component.set("v.contacts", a.getReturnValue());
         });
         $A.enqueueAction(action);
     },
    
    /**
     * You first get the value of the new searchKey available in the event object.
     * You then invoke the findByName() method in the Apex controller you created in module 3.
     * When the asynchronous call returns, you assign the list of contacts returned by 
     * findByName() to the component’s contacts attribute.
    */
    
    searchKeyChange: function(component, event) {
         var searchKey = event.getParam("searchKey");
         var action = component.get("c.findByName");
         action.setParams({
           "searchKey": searchKey
         });
         action.setCallback(this, function(a) {
            component.set("v.contacts", a.getReturnValue());
         });
         $A.enqueueAction(action);
     }
 })