({
	handleClick : function(component, event, helper) {
        var accountName = component.get("v.account.Name");
        var action = component.get("c.isDuplicateName");
        action.setParams({
            accountName: accountName
        });
        // Create a callback that is executed after the server-side action returns
        action.setCallback(this, function(data) {
            if(data.getReturnValue()) {
               	var response = confirm('Account already exists. Do you want to create duplicate Account');
                if(response) {
					helper.addAccount(component, accountName);
                } 
            }else{
                helper.addAccount(component, accountName);
            }
        });
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);        
	}
})