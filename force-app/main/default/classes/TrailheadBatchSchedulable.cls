/*
 * File Name   : TrailheadBatchSchedulable 
 * Description : Global class to implement shedulable that call the batch class TrailheadCalloutBatch
 * 
 * Modification Log
 * ======================================================== 
 * Ver Date       Author                   Modification
 * --- ---------- --------------           --------------------------
 * 1.0 16/02/2019 ACN victor.tz.ng         Initial version
 */

global class TrailheadBatchSchedulable implements Schedulable {
   global void execute(SchedulableContext sc) {
      TrailheadCalloutBatch thc = new TrailheadCalloutBatch(); 
      Database.executebatch(thc,5); //set batch size to 5
   }
}