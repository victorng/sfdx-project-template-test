public without sharing class CampaignMemberVFControllerExt{

    private CampaignMember cMember;

    public CampaignMemberVFControllerExt (ApexPages.StandardController controller) {

        List<String> fields = new List<String>();

        fields.add('Id');
        fields.add('campaignId');
        fields.add('FirstName');
        fields.add('LastName');
        fields.add('Status');
        fields.add('Email');
        fields.add('Double_Check_Counter__c');
        fields.add('Attended__c');
        fields.add('AttendanceRecordedTime__c');
        fields.add('Campaign.EventStartDatetime__c');
        fields.add('Campaign.EventEndDatetime__c');

        if (!Test.isRunningTest()) { 
            controller.addFields(fields);
        }
        this.cMember = (CampaignMember)controller.getRecord();

    }

    public void updateAttendance(){
        try {
            DateTime currentTime = Datetime.now();

            // Attendee is unregistered as not within event time
            if ((currentTime < cMember.Campaign.EventStartDatetime__c.addHours(-2) ||
                currentTime > cMember.Campaign.EventEndDatetime__c) &&
                cMember.Attended__c == false) {
                cMember.Double_Check_Counter__c = -1;
                update cMember;
            // First time registered attendee
             } else if (currentTime >= cMember.Campaign.EventStartDatetime__c.addHours(-2) &&
                        currentTime <= cMember.Campaign.EventEndDatetime__c &&
                        cMember.Attended__c == false){
                cMember.Attended__c = true;
                if (cMember.AttendanceRecordedTime__c == null) {
                    cMember.AttendanceRecordedTime__c = currentTime;
                }
                cMember.Double_Check_Counter__c = 0;
                update cMember;
            // Attendee already registered scans again
            } else if (cMember.Attended__c == true &&
                       cMember.AttendanceRecordedTime__c != null) {
                cMember.Double_Check_Counter__c++;
                update cMember;
            }

        } catch (Exception e){
            System.debug(e);
        }
    }
    
    public CampaignMember getcMember() {
        return cMember;
    }
}