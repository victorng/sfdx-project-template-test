public class ContactAndLeadSearch {
    public static List<List<SObject>> searchContactsAndLeads(String str) {
        List<List<SObject>> searchList = [FIND 'Smith' IN NAME FIELDS
                                          RETURNING Contact(Name), Lead(Name)];
        return searchList;
    }
}