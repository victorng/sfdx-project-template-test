public with sharing class SurveyHomeController {
	
	public SurveyHomeController(){
	}
	
	public List<Survey__c> getAllSurvey(){
		return [SELECT Id,Name,Description__c FROM Survey__c Order by Name];
	}
	
}