public with sharing class SurveyPreviewController {
	private Survey__c survey;


    public SurveyPreviewController(ApexPages.StandardController controller) {
		this.survey = (Survey__c)controller.getRecord();
    }
    
    public List<Survey_Question__c> getAllQuestions(){
	  return [SELECT Question__r.Question_Text__c, Question__r.Name, Question__c 
               FROM Survey_Question__c WHERE Survey__c = :this.survey.Id];
             
    }

}