@isTest
private class AccountProcessorTest{
    @isTest
    static void testAccountProcessor(){
        Account a = new Account();
        a.Name = 'AccountTest1';
        insert a;
        
        Contact c = new Contact();
        c.FirstName = 'ContactTest';
        c.LastName = '1';
        c.AccountId = a.Id;
        insert c;
        
        Set<id> setAccountId = new Set<id>();
        setAccountId.add(a.id);
        
        Test.startTest();
            AccountProcessor.countContacts(setAccountId);
        Test.stopTest();
        
        Account acc = [SELECT Number_of_Contacts__c FROM Account WHERE id = :a.id LIMIT 1];
        System.assertEquals(1, Integer.valueOf(acc.Number_of_Contacts__c));
    }
}