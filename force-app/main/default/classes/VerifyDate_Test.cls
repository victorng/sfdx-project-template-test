@isTest
public class VerifyDate_Test {
    
    @isTest static void testDateWithin30Days(){
        Date date1 = Date.newInstance(2020, 8, 31);
        Date date2 = Date.newInstance(2020, 9, 20);
        System.assertEquals(VerifyDate.CheckDates(date1, date2), date2);
    }
    
    @isTest static void testDateOutside30Days(){
        Date date1 = Date.newInstance(2006, 8, 31);
        Date date2 = Date.newInstance(2016, 9, 20);
        Integer totalDays = Date.daysInMonth(date1.year(), date1.month());
        Date lastDay = Date.newInstance(date1.year(), date1.month(), totalDays);
        System.assertEquals(VerifyDate.CheckDates(date1, date2), lastDay);
    }
}