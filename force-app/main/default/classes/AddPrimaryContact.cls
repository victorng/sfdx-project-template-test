public class AddPrimaryContact implements Queueable {

    private Contact c;
    private String state;
    
    public AddPrimaryContact(Contact c, String billingState){
        this.c = c;
        this.state = billingState;
    }
    
    public void execute(QueueableContext context){
        
        List<Account> accounts = [SELECT ID, Name, (SELECT ID, FirstName, LastName FROM Contacts) FROM Account WHERE BillingState__c = :state LIMIT 200];
        List<Contact> contacts = new List<Contact>();
        for (Account a : accounts){
            Contact contact = c.clone(false,false,false,false);
            contact.AccountId = a.Id;
            contacts.add(contact);
        }
        if (contacts.size() > 0){
            insert contacts;
        }
    }
}