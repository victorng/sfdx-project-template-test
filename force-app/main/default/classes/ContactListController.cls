/**ContactListController is a regular controller class with methods to retrieve contacts
 *(findAll), or to search contacts by name (findByName) or by id (findById).
 *The @AuraEnabled method annotation makes a method available to Lightning applications
**/

public with sharing class ContactListController {

     @AuraEnabled
     public static List<Contact> findAll() {
         return [SELECT id, name, phone FROM Contact LIMIT 50];
     }

     @AuraEnabled
     public static List<Contact> findByName(String searchKey) {
         String name = '%' + searchKey + '%';
         return [SELECT id, name, phone FROM Contact WHERE name LIKE :name LIMIT 50];
     }

     @AuraEnabled
     public static Contact findById(String contactId) {
         return [SELECT id, name, title, phone, mobilephone, Account.Name
                     FROM Contact WHERE Id = :contactId];
     }


 }