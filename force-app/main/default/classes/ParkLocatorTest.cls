@isTest
private class ParkLocatorTest {
    @isTest static void testCallout() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new ParkServiceMock());
        // Call the method that invokes a callout
        String countryName = 'Japan';
        String [] result = ParkLocator.country(countryName);
        
        // Verify that a fake result is returned
        System.assertEquals(new List<String>{'Shiretoko National Park','Bandai-Asahi National Park', 'Hakusan National Park'}, result); 
    }
}