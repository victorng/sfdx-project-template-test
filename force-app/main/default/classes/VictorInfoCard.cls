global virtual with sharing class VictorInfoCard extends VictorInfoCardController {
    //returning markup defined in the controller class
    global override String getHTML() {
    return infoCardHTML();
    }
}