@isTest
public class Test_AccountController {
    static testmethod void createAccount(){
        
        Integer accCountBefore = [SELECT Count() from Account];

        Account acc = new Account (
            Name = 'Dummy Account'
        );
        
        Test.startTest();
        insert acc;
        Test.stopTest();
        
        Integer accCountAfter = [SELECT Count() from Account];
        
        System.assertEquals(accCountBefore + 1, accCountAfter);
    }
}