public with sharing class UnauthVFController {

    private CampaignMember cmTemp;

    public UnauthVFController (ApexPages.StandardController controller) {
        List<String> fields = new List<String>();

        fields.add('Firstname');
        fields.add('Lastname');
        fields.add('Email');
        fields.add('Attended__c');
        controller.addFields(fields);

        this.cmTemp = (CampaignMember)controller.getRecord();
    }
    
    public void updateAttendance(){
        this.cmTemp.Attended__c = true;
        update this.cmTemp;
    }

    public CampaignMember getcmTemp() {
        return cmTemp ;
    }

    
}