public class NewCaseListController {
    String newStatus = 'New';
    public List<Case> getNewCases() {
        List<Case> results = Database.query(
            'SELECT ID, CaseNumber FROM Case WHERE Status = :newStatus');
        return results;
    }
}