@isTest
private class AccountManagerTest {
    @isTest
    static void testGetAccount(){
        Id recordId = createTestRecord();
        //Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri = 'https://victorng-dev-ed.my.salesforce.com/services/apexrest/Accounts/' + recordId + '/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;
        
        // Call the method to test
        Account acc = AccountManager.getAccount();
        
        // Verify results
        System.assert(acc != null);
        System.assertEquals('AccountTest1', acc.Name);
    }

    // Helper method
    static Id createTestRecord() {
        // Create test record
        Account accountTest = new Account(
            Name='AccountTest1');
             
        insert accountTest;
        
        Contact contactTest = new Contact(
            FirstName='ContactTest1',
            AccountId = accountTest.id);  
         
        return accountTest.Id;
    }   
}