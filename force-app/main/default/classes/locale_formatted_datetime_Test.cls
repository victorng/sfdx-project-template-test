/*
* Class Name: locale_formatted_datetime_Test  
* Project Name: National Gallery CR0006
* @description: Test Class for locale_formatted_datetime
* Created by: Victor Ng
* Created Date: 8-Nov-2017
* Modification Log
* ======================================================== 
* Date         Author                      Modification
* ----------  ------------------------    --------------------------
* 8-Nov-2017  (Accenture) victor.tz.ng     Initialize Test Class
*/

@isTest
public class locale_formatted_datetime_Test {
    static testMethod void TestTimeZone(){

        //Test Singapore format
        List<User> userUpdate = new List<User>();
        userUpdate.add(new User(Id = UserInfo.getUserId(), LocaleSidKey = 'en_SG'));
        update userUpdate;
        
        locale_formatted_datetime controller = new locale_formatted_datetime();
        controller.date_time = DateTime.valueOf('1965-08-09 21:30:12'); 
        String test_value = controller.getTimeZoneValue();
        System.assertEquals('09-Aug-1965, 21:30 SGT', test_value);
    }
}