public without sharing class locale_formatted_datetime {
    
    public DateTime date_time { get; set; } 

    public String getTimeZoneValue() {
            Map<String, String> mappedValues = new Map<String, String>(); //map for holding locale to datetime format
            mappedValues = MapValues();
            String user_locale = UserInfo.getLocale();
            String datetime_format = 'dd-MMM-yyyy, H:mm z';
            if (mappedValues.containsKey(user_locale)) {
                datetime_format = mappedValues.get(user_locale);
            }
            String locale_formatted_date_time_value = date_time.format(datetime_format);
            
            return locale_formatted_date_time_value;
        }
        
        //populate a map with locale values and corresponding datetime formats
    private Map<String, String> MapValues() {
        Map<String, String> locale_map = new Map<String, String>();
        locale_map.put('en_SG', 'dd-MMM-yyyy, H:mm z');
        locale_map.put('en_US', 'MMM-dd-yyyy, H:mm z');
        
        return locale_map;
    }
}