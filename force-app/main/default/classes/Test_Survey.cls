@isTest
private class Test_Survey {
  
  @isTest
  private static void Test_SurveyHomeController() {
     Survey__c survey = new Survey__c(Name = 'Survey1',Description__c = 'A simple survey used for testing');
     insert survey;
     
     SurveyHomeController surveyHomeController = new SurveyHomeController();
     List<Survey__c> lstAllSurvey = surveyHomeController.getAllSurvey();
     System.assertEquals(lstAllSurvey.size(),1);
     System.assertEquals(lstAllSurvey[0].Name,'Survey1');
     
  }
  
  @isTest
  private static void Test_SurveyPreviewController() {
     Survey__c survey = new Survey__c(Name = 'Survey1',Description__c = 'A simple survey used for testing');
     insert survey;
     
     Question__c question = new Question__c(Name = 'Favourite Color ?',Question_Text__c = 'Your favourite color');
     insert question;
     
     Survey_Question__c surveyQuestion = new Survey_Question__c(Question__c = question.Id,Survey__c = survey.Id);
     insert surveyQuestion;
     
     ApexPages.StandardController sController = new ApexPages.StandardController(survey);
     SurveyPreviewController surveyPreviewController = new SurveyPreviewController(sController);
     
     List<Survey_Question__c> lstSurveyQuestion = surveyPreviewController.getAllQuestions();
     System.assertEquals(lstSurveyQuestion.size(),1);
 
  } 
}