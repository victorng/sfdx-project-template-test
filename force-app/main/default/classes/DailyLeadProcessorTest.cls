@isTest
public class DailyLeadProcessorTest {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
	@testSetup
    static void setup(){
        List<Lead> leads = new List<Lead>();
        for (Integer i=0; i<200; i++){
            leads.add(new Lead(FirstName = 'Lead', LastName = 'i', Company = 'ApexSchedulerTrailhead'));
        }
        insert leads;
    }
    static testmethod void test(){
        Test.startTest();
        String jobId = System.schedule('ScheduledApexTest', CRON_EXP, new DailyLeadProcessor());
        Test.stopTest();
    }
}