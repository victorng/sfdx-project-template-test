global class LeadProcessor implements Database.Batchable<sObject>, Database.Stateful {
    
    // instance member to retain state across transactions
    global Integer recordsProcessed = 0;
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(
            'SELECT ID, LastName FROM Lead'
            );
    }
    global void execute (Database.BatchableContext bc, List<Lead> leads){
        //process each batch of records
        for (Lead l : leads){
            l.LeadSource = 'Dreamforce';
        }
        update leads;
    }
    global void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' records processed!');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                           FROM AsyncApexJob
                           WHERE Id = :bc.getJobId()];
        System.debug(job);
    }
}