/*
 * File Name   : TrailheadCalloutBatch
 * Description : HTTPRequest call Trailhead Profile Page using Batch Apex
 * 
 * Modification Log
 * ======================================================== 
 * Ver Date       Author                   Modification
 * --- ---------- --------------           --------------------------
 * 1.0 14/02/2019 ACN victor.tz.ng         Initial version
 * 1.1 18/02/2019 ACN wen.jun.zhu          Optimzied Code
 */

global class TrailheadCalloutBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
        String query = 'SELECT Id, Name, TrailheadID__c FROM Trailblazer__c';
        return Database.getQueryLocator(query); 
    }

    global void execute(Database.BatchableContext BC, List<Trailblazer__c> lstRecords) {
        
        for (Trailblazer__c tb : lstRecords) {
            HttpRequest req = new HttpRequest();
            //req.setEndpoint('https://docsample.herokuapp.com/xmlSample');
            //req.setEndpoint('https://trailhead.salesforce.com/en/me/005500000086mVCAAY');
            req.setEndpoint('https://trailhead.salesforce.com/en/me/' + tb.TrailheadID__c);
            req.setMethod('GET');
            Http http = new Http();
            HttpResponse res = http.send(req);
            String strBody = res.getBody();

            String t1 = strBody.substringAfter('<div class=\'user-information__achievements-data\' data-test-badges-count>');
            String badgeCount = t1.substringBefore('</div>');
            System.debug(badgeCount + ' badges');
            tb.BadgeCount__c = Integer.valueOf(badgeCount);

            String t2 = strBody.substringAfter('data-test-points-count>\n');
            String points = t2.substringBefore('\n</div>').replace(',',''); //points is string because of the ,
            System.debug(points + ' points');
            tb.Points__c = Integer.valueOf(points);

            String t3 = strBody.substringAfter('data-test-trails-count>\n');
            String trailCount = t3.substringBefore('\n</div>');
            System.debug(trailCount + ' trails ');
            tb.TrailsCompleted__c = Integer.valueOf(trailCount);

            /* Email is only visible to user themself
            String t4 = strBody.substringAfter('email&quot;:&quot;');
            String acnEID = t4.substringBefore('@accenture.com&quot;');
            System.debug(acnEID + ' acnEID');
            tb.Email__c = acnEID + '@accenture.com';
            */
            
            /* Will use Name from Accenture System not Trailhead
            String t5 = strBody.substringAfter('first_name&quot;:&quot;');
            String firstName = t5.substringBefore('&quot;,&quot;last_name');
            System.debug(firstName + ' firstName');
            tb.FirstName__c = firstName;

            String t6 = strBody.substringAfter('last_name&quot;:&quot;');
            String lastName = t6.substringBefore('&quot;,&quot;job_role');
            System.debug(lastName + ' lastName');
            tb.LastName__c = lastName;
            */
        }
        try {
            UPSERT lstRecords;
        }
        catch(Exception e) {
            System.debug(e);
        }
    }

    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
}