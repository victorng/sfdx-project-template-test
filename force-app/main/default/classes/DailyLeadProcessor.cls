global class DailyLeadProcessor implements Schedulable {
    
    global void execute(SchedulableContext sc){
        List<Lead> leads = [SELECT ID, LastName FROM Lead WHERE LeadSource = '' LIMIT 200];
        for (Lead l : leads){
            l.LeadSource = 'Dreamforce';
        }
        update leads;
    }
}