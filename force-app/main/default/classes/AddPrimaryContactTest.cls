@isTest
public class AddPrimaryContactTest {
	@testSetup
    static void setup(){
        List<Account> accounts = new List<Account>();
        for (Integer i=0; i<50; i++){
            accounts.add(new Account(name='Test Queueable NY' + i, BillingState__c ='NY'));
        }
        for (Integer i=0; i<50; i++){
            accounts.add(new Account(name='Test Queueable CA' + i, BillingState__c ='CA'));
        }
        insert accounts;
    }
    
    static testmethod void testQueueable(){
  		Contact contact = new Contact();
        contact.FirstName = 'Test Contact Queueable';
        contact.LastName = '1';
        insert contact;
        
        String state = 'CA';
        
        //Create queueable instance
        AddPrimaryContact instance = new AddPrimaryContact(contact, state);

        Test.startTest();
        System.enqueueJob(instance);
        Test.stopTest();
        
        System.assertEquals(50, [SELECT Count() FROM Account WHERE BillingState__c = 'CA']);
    }
}