@isTest
private class TestRestrictContactByName {
    @isTest static void testBlockInsertContact() {
        //Test both valid and invalid cases by adding them to List
        List<Contact> listContact= new List<Contact>();
        Contact c1 = new Contact(FirstName = 'Victor', LastName = 'Ng');
        Contact c2 = new Contact(FirstName = 'Vic', LastName = 'INVALIDNAME');
        listContact.add(c1);
        listContact.add(c2);
        
        Test.startTest();
        insert listContact;
        Test.stopTest();
        
    }
}