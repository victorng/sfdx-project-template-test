global virtual with sharing class VictorInfoCardController extends cms.ContentTemplateController {
//extend the cms.ContentTemplateController for all custom content
    // Constructor for Edit Page
    global VictorInfoCardController ( cms.CreateContentController cc) {
        super(cc);
    }
    // Default Constructor
    global VictorInfoCardController (){}
    // getHTML method for returning programmatically generated HTML to the Generate Page
    global virtual override String getHTML() { // For generated markup
    return '';
    }
    // Methods to get the attribute values for the content
    public String thumbnail {
    get {
    return getProperty('thumbnail');
    }
    set;
    }
    //Basic return of attribute value
    public String fullName {
    get {
    return getProperty('fullName');
    }
    set;
    }
    //Example of return accounting for possibility value was not supplied
    public String title {
    get {
    string retValue = String.isNotBlank(getProperty('title')) ? getProperty('title') : '';
    return retValue;
    }
    set;
    }
    public String email {
    get {
    return getProperty('email');
    }
    set;
    }
    public String workAddress {
    get {
    return getProperty('workAddress');
    }
    set;
    }
    public String website {
    get {
    return getProperty('website');
    }
    set;
    }
    public String bio {
    get {
    return getProperty('bio');
    }
    set;
    }
    // Method to parse the value returned from website
    public cms.Link parsedWebsite{
        get {
            return new cms.Link(this.website, this.page_mode, this.site_name);
        }
    }
    //Method to return value of link to display as visible value to visitor
    public string linkName{
        get {
            cms.Link lk = new cms.Link(this.website, this.page_mode, this.site_name);
            return lk.getLinkName();
        }
    }
    //method for outputting the generated markup
    public String infoCardHTML() {
    String html = '';
    html += '<div class="infoCard">';
    html += '<img src="' + thumbnail + '"/>';
    html += '<div class="infoSection">';
    html += '<div class="fullName">' + fullName + '</div>';
    html += '<div class="title">' + title + '</div>';
    html += '<div class="workAddress">' + workAddress + '</div>';
    html += '<div class="email">' + email + '</div>';
    html += '<a href="';
    if(this.parsedWebsite.targetPage != null)
    html += parsedWebsite.targetPage;
    html += '"';
    if(this.parsedWebsite.target != null && this.parsedWebsite.target != '')
    html += ' target="'+this.parsedWebsite.target+'"';
    if(this.parsedWebsite.javascript != null && this.parsedWebsite.javascript != '')
    html += ' onclick=\''+this.parsedWebsite.javascript+'\'';
    html += '>';
    html += linkName +'</a>';
    html += '</div>';
    html += '<div class="bio">' + bio + '</div>';
    html += '</div>';
    return html;
    }
}