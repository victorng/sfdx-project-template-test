public class AccountProcessor {
    @future
    public static void countContacts(Set<id> setId){
        List<Account> accountIds = [SELECT id, Number_of_Contacts__c, (SELECT id FROM Contacts) FROM Account WHERE id IN :setId];
        for (Account a : accountIds){
            List<Contact> contactList = a.contacts;
            a.Number_Of_Contacts__c = contactList.size();
        }
        update accountIds ;
    }
}